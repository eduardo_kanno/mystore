<?php

class CompanyControllerCore extends FrontController
{
    public $php_self = 'company';

    public function initContent()
    {
        parent::initContent();

        $this->setTemplate(_PS_THEME_DIR_.'company.tpl');
    }
}