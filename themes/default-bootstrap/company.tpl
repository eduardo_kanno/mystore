<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</head>

<body>

<div class="row">
    <h3>Conheça a empresa</h3>

    <br>

    <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In mattis enim at est molestie eleifend. Nullam id suscipit tortor. Donec lectus nulla, tincidunt ut accumsan sed, dapibus sed libero. Duis sapien mi, bibendum quis posuere non, consequat ac magna. Aenean in suscipit ante. Donec vitae bibendum felis. Aliquam posuere finibus tortor sit amet vehicula. Nam purus odio, tempus sit amet lectus vel, convallis gravida lectus. Nam augue ipsum, varius non velit at, feugiat luctus elit. Donec imperdiet nunc et ornare dignissim. Fusce cursus nec nisi vitae tristique. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>

    <p> Aliquam luctus sapien nec purus finibus elementum. Integer pretium, mauris id egestas ullamcorper, turpis tortor sodales erat, sit amet tristique felis justo vitae elit. Nulla facilisi. Cras vitae odio et arcu congue facilisis vel eu magna. Cras eget orci at turpis imperdiet lacinia a eu nisl. Donec in neque est. In gravida nec metus sed bibendum. Sed eu dignissim magna. Praesent posuere ex ultricies erat tincidunt egestas. Quisque fringilla, leo ac auctor bibendum, ipsum lorem dapibus ante, vel posuere sem dolor sit amet eros.</p>

    <p> Nulla ac metus sit amet orci fermentum mollis vitae ac ipsum. Proin placerat tempor massa, ac sodales tortor congue quis. Quisque placerat quam accumsan, molestie augue elementum, consequat felis. Proin semper tempor purus, sit amet sodales diam. Cras et leo vehicula, dapibus risus sed, tempor sem. Duis at efficitur turpis. Proin lacinia mi in ante faucibus pulvinar. Maecenas mollis justo vitae lobortis feugiat. Phasellus interdum ante id consequat lobortis. Donec non nunc laoreet, mollis massa in, cursus elit. Aliquam dapibus lorem est, ut malesuada nisl placerat nec. Nam at auctor nisl, at euismod eros. Suspendisse tempus a purus at suscipit. In hac habitasse platea dictumst. Etiam tincidunt justo vel ligula rhoncus vestibulum. Nulla enim mi, accumsan id efficitur at, commodo a leo.</p>

    <p> Nulla at laoreet dolor. Sed mattis sem quis arcu posuere tempor. Maecenas eros ipsum, posuere ac fringilla quis, pellentesque ut risus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin placerat massa sem, eget porta nisi interdum eu. Proin pharetra, neque a condimentum viverra, nibh velit tincidunt nibh, ut lobortis lorem nulla vel tellus. Nulla sit amet elit vitae sem sagittis iaculis quis eget augue. Pellentesque non tortor et lorem eleifend dapibus. Proin convallis nulla eu consectetur aliquam. Quisque nec tempus urna. Pellentesque lobortis, ex vel volutpat pellentesque, enim lectus eleifend sapien, nec fringilla dui enim vitae sapien. Duis sed maximus nisl. Aenean ultrices tortor in finibus posuere. Suspendisse enim ipsum, placerat a aliquet faucibus, aliquam in velit. Mauris massa sem, euismod ac bibendum eu, elementum at ipsum. Duis laoreet auctor diam, rutrum vehicula elit consectetur vitae.</p>

</div>


<div class="row">
    <div class="centraliza">
        <div class="col-xs-12">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/yTzmPuApljw" frameborder="0" allowfullscreen></iframe>
        </div>
    </div>
</div>

<br>

<span>Confira o Making Of : </span>

<div class="row">
    <div class="centraliza">
        <div class="col-xs-12">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/LkpM3pcGEjY" frameborder="0" allowfullscreen></iframe>
        </div>
    </div>
</div>



</body>


<style>
    .centraliza {
        width: 555px;
        margin: auto;
        height: 375px; //Se quer altura automática, é só deletar essa linha
    }
</style>

</html>